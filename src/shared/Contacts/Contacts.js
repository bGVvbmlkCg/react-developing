import React from "react";
import "./Contacts.scss";

const Contacts = ({email, phone}) => {
    const phoneLink = "tel:" + phone.replace(" ", "");
    const emailLink = "mailto::" + email;

    return (
        <div className="Contacts d-flex flex-column align-items-end h-100">
            <a href={emailLink}>{email}</a>
            <a href={phoneLink}>{phone}</a>
        </div>
    )
}

export default Contacts;