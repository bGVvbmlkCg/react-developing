import React from "react";
import image from "./logo-sprite.png";
import "./Logo.scss";

const logoIconStyles = {
    backgroundImage: `url(${image})`,
}

const Logo = ({holiday_place}) => {
    return (
        <a href="/" className="Logo d-flex align-items-center">
            <i className="icon" style={logoIconStyles}/>
            <span className="d-flex flex-column ml-3 text-uppercase">
                <span>{holiday_place}</span>
                <span>holidays</span>
            </span>
        </a>
    )
}

export default Logo;